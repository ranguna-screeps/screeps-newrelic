#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [ -f "$SCRIPT_DIR/load-local-secrets.sh" ]; then
	. $SCRIPT_DIR/load-local-secrets.sh
fi
