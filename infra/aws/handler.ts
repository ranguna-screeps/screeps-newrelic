import {collectStatistics} from "../../src/collectStatistics";
import {SSM} from "aws-sdk";

const ssm = new SSM();

export const handler = async() => {
	const unparsedParameterValue = await ssm
		.getParameter({
			Name: `/service/${process.env.SERVICE_NAME!}`
		})
		.promise()
		.catch((err) => {
			console.error('Failed getting parameter');
			console.error(err);
			throw err;
		});
	const parameters = JSON.parse(unparsedParameterValue.Parameter!.Value!);

	process.env.SCREEPS_TOKEN = parameters.screepsToken;
	process.env.NEW_RELIC_LICENSE_KEY = parameters.newRelicLicenseKey;
	process.env.SCREEPS_SHARD = parameters.screepsShard;

	await (await import("../../src/collectStatistics")).collectStatistics();
};