# Welcome to your CDK TypeScript project!

This is a blank project for TypeScript development with CDK.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Setup

1. Create an aws account;
2. Follow this guide to setup credentials for a local profile tagged as "screeps": https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html -> pragmatic access;
3. Run `npm run deploy:aws`;
4. ...
5. Profit!

## Cost

AWS offer 1M free lambda executions per month, so if this is the only thing you run in your aws account, you'll only be charged for egress, which depends on your the amount of metrics you have.

## Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template
