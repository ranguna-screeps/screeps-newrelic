import {LambdaFunction} from '@aws-cdk/aws-events-targets';
import {Rule, Schedule} from '@aws-cdk/aws-events';
import {Architecture, Runtime} from '@aws-cdk/aws-lambda';
import {NodejsFunction} from '@aws-cdk/aws-lambda-nodejs';
import {StringParameter} from '@aws-cdk/aws-ssm';
import * as cdk from '@aws-cdk/core';
// import * as sqs from '@aws-cdk/aws-sqs';

export class AwsStack extends cdk.Stack {
  screepsAnalyticsAgentLambda: NodejsFunction;
  screepsAnalyticsAgentSchedule: Rule;

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    if (!process.env.SCREEPS_TOKEN) throw new Error("SCREEPS_TOKEN environmental variable needs to be defined.");
    if (!process.env.SCREEPS_SHARD) throw new Error("SCREEPS_SHARD environmental variable needs to be defined.");
    if (!process.env.NEW_RELIC_LICENSE_KEY) throw new Error("NEW_RELIC_LICENSE_KEY environmental variable needs to be defined.");

    const serviceName = scope.node.tryGetContext("name");
    this.screepsAnalyticsAgentLambda = new NodejsFunction(this, "screepsAnalyticsAgent", {
      architecture: Architecture.ARM_64,
      functionName: "screeps-analytics-agent",
      memorySize: 128,
      retryAttempts: 0,
      runtime: Runtime.NODEJS_14_X,
      entry: 'handler.ts',
      environment: {
        SERVICE_NAME: serviceName,
      }, 
    });
    this.screepsAnalyticsAgentSchedule = new Rule(
      this,
      `screepsAnalyticsAgentSchedule`,
      {
        schedule: Schedule.rate(cdk.Duration.minutes(1)),
      },
    );
    this.screepsAnalyticsAgentSchedule.addTarget(new LambdaFunction(this.screepsAnalyticsAgentLambda));

    const parameterStore = new StringParameter(this, "ParameterStore", {
      parameterName: `/service/${serviceName}`,
      stringValue: JSON.stringify({
        "screepsToken": process.env.SCREEPS_TOKEN,
        "screepsShard": process.env.SCREEPS_SHARD,
        "newRelicLicenseKey": process.env.NEW_RELIC_LICENSE_KEY
      })
    });

    parameterStore.grantRead(this.screepsAnalyticsAgentLambda);
  }
}
