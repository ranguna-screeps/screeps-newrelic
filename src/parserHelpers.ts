export const objectToDotNotation = (object: object, keyParent = ""): Record<string, any> =>
	Object.entries(object).reduce(
		(record, [key, value]) => ({
			...record,
			...(
				typeof value === "object" && !Array.isArray(value) ?
					objectToDotNotation(value, keyParent + (keyParent ? "." : "") + key)
					: {
						[keyParent + "." + key]: value
					}
			)
		}),
		{}
	);