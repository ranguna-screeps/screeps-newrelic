import {GaugeMetric} from "@newrelic/telemetry-sdk/dist/src/telemetry/metrics";
import {publishMetrics} from "./metricPublisher";
import {objectToDotNotation} from "./parserHelpers";
import {collectScreepsMetricsMemory} from "./screepsService";

export const collectStatistics = async() => {
	try {
		console.log("Collecting");
		const screepsMetricMemory = await collectScreepsMetricsMemory();

		console.log("Publishing", JSON.stringify(screepsMetricMemory, undefined, 2));

		const metrics = Object.entries(objectToDotNotation(screepsMetricMemory, 'screeps'))
			.flatMap(([metricPathName, metricValue]) => Array.isArray(metricValue) ?
				metricValue.map(metricWithAttributes => new GaugeMetric(metricPathName, metricWithAttributes.value, metricWithAttributes.attributes))
				: new GaugeMetric(metricPathName, metricValue)
			);

		publishMetrics(metrics);
	} catch(err) {
		console.error(`There was an error uploading metrics for tick`, err);
	}
}
