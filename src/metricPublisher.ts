import {Metric, MetricBatch, MetricClient} from "@newrelic/telemetry-sdk/dist/src/telemetry/metrics";

if (!process.env.NEW_RELIC_LICENSE_KEY)
	throw new Error("Missing new relic license key.");

export const client = new MetricClient({
	apiKey: process.env.NEW_RELIC_LICENSE_KEY,
	host: process.env.NR_HOST ?? "metric-api.eu.newrelic.com"
});

export const publishMetrics = (metrics: Metric[]) => {
	const batch = new MetricBatch();
	metrics.forEach(metric => batch.addMetric(metric));

	return new Promise((resolve, reject) =>
		client.send(batch, (err, _, body) => {
			if (err)
				return reject(err);

			return resolve(body);
		})
	);
}
