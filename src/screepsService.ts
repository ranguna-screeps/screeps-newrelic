import axios from "axios";
import {unzip} from "zlib";

if (!process.env.SCREEPS_TOKEN)
	throw new Error("Missing screeps auth token.");

if (!process.env.SCREEPS_SHARD)
	throw new Error("Missing screeps shard.");

const axioScreeps = axios.create({
	baseURL: "https://screeps.com/api/",
	headers: {
		'X-Token': process.env.SCREEPS_TOKEN
	},
});

interface GetMemoryResponse {
	data: string;
}

export const collectScreepsMetricsMemory = () =>
	axioScreeps.get<GetMemoryResponse>(
		"/user/memory",
		{
			params: {
				path: process.env.METRICS_MEMORY_PATH ?? 'custom.metrics',
				shard: process.env.SCREEPS_SHARD
			},
		}
	).then(res => new Promise<object>((resolve, reject) =>
		unzip(
			Buffer.from(res.data.data.split(':')[1], 'base64'),
			(err, result) => err ?
				reject(err)
				: resolve(
					JSON.parse(result.toString())
				)
		)
	));