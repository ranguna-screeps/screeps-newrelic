import {collectStatistics} from "./collectStatistics";

setInterval(
	async() => {
		await collectStatistics();
	},
	61 * 1000
)
